require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  def setup
    @user = User.new(name: 'User',
                     email: 'user@example.com')
  end
  
  
  test 'name should be present' do
    @user.name = '  '
    assert_not @user.valid?
  end
  
  test 'name should not be too long' do
    @user.name = 'a' * 21
    assert_not @user.valid?
  end
  
  test 'email should be present' do
    @user.email = '  '
    assert_not @user.valid?
  end
  
  
  test 'email should not be too long' do
    @user.email = 'a' * 201
    assert_not @user.valid?
  end
  
  test 'password should be greater than 6 characters' do
    @user.password = "a" * 5 
    assert_not @user.valid?
  end
  
  test 'password should be less than 60 characters' do
    @user.password = 'a' * 61
    assert_not @user.valid?
  end
  
  test 'email should be unique' do
    duplicate_user = @user.dup
    @user.save
    assert_not duplicate_user.valid?
  end
  
  
end
