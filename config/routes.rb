Rails.application.routes.draw do

  get 'items/index'

  get 'items/new'

  get 'items/show'

  get 'items/edit'

  get 'sessions/new'

    get root 'general_pages#index'
    
    resources :users 
    
    resources :items
    
    resources :vegetables
    
    resources :fruits
    
    resources :juices
    
    get      '/faq',             to:  'faq#general_pages'
    
    get      '/help',            to:  'help#general_pages'
    
    get      '/about',           to:  'about#general_pages'

    get      '/cart',            to:  'carts#index'
    
    get      '/clearcart',       to:  'carts#clearcart'

    get      '/cart/:id',        to:  'carts#add'
    
    get      '/question',        to:  'items#questions'
    
    get      '/recommendation',  to:  'items#recommendations'
    
    get      '/juice_table',     to:  'items#table'
    
    get      '/login',           to:  'sessions#new'
    
    post     '/login',           to:  'sessions#create'
    
    get      '/logout',          to:  'sessions#destroy'
    
    get      '/store',           to:  'general_pages#store'
    
    get      '/sort',            to:  'items#sort'   
    
#    get     '/search',          to:  'items#search'

    
    
end
