# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160727092437) do

  create_table "fruits", force: :cascade do |t|
    t.string   "title"
    t.integer  "price"
    t.text     "description"
    t.string   "url"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "items", force: :cascade do |t|
    t.string   "title"
    t.float    "price"
    t.text     "description"
    t.string   "catergory"
    t.string   "url"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "url_2"
    t.string   "url_3"
    t.string   "url_4"
    t.string   "url_1"
    t.integer  "sugar_comtent"
    t.integer  "fat_level"
    t.boolean  "contains_nuts"
    t.string   "mixture"
    t.integer  "valueCalories"
    t.integer  "valueFatCalories"
    t.integer  "valueTotalFat"
    t.integer  "valueSatFat"
    t.integer  "valueTransFat"
    t.integer  "valueCholesterol"
    t.integer  "valueSodium"
    t.integer  "valueTotalCarb"
    t.integer  "valueFibers"
    t.integer  "valueSugars"
    t.integer  "valueProteins"
    t.integer  "valueVitaminA"
    t.integer  "valueVitaminC"
    t.integer  "valueCalcium"
    t.integer  "valueIron"
    t.text     "ingredients"
    t.integer  "protein_level"
  end

  create_table "juices", force: :cascade do |t|
    t.string   "title"
    t.integer  "price"
    t.text     "description"
    t.string   "url"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "products", force: :cascade do |t|
    t.string   "title"
    t.string   "price"
    t.text     "description"
    t.string   "url"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "password_digest"
    t.boolean  "admin",           default: false
    t.string   "remember_digest"
  end

  create_table "vegetables", force: :cascade do |t|
    t.string   "title"
    t.integer  "price"
    t.text     "description"
    t.string   "url"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

end
