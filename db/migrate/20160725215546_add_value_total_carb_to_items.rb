class AddValueTotalCarbToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :valueTotalCarb, :integer
  end
end
