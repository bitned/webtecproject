class AddValueFatCaloriesToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :valueFatCalories, :integer
  end
end
