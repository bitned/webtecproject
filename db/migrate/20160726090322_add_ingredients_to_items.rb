class AddIngredientsToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :ingredients, :text
  end
end
