class AddMixtureToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :mixture, :string
  end
end
