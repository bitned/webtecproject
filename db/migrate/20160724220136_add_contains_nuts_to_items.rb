class AddContainsNutsToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :contains_nuts, :boolean
  end
end
