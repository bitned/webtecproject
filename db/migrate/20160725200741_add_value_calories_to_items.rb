class AddValueCaloriesToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :valueCalories, :integer
  end
end
