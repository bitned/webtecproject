class AddFatLevelToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :fat_level, :integer
  end
end
