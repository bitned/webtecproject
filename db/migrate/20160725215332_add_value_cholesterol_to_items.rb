class AddValueCholesterolToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :valueCholesterol, :integer
  end
end
