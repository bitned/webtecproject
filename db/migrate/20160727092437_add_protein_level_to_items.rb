class AddProteinLevelToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :protein_level, :integer
  end
end
