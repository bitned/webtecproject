class AddValueTotalFatToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :valueTotalFat, :integer
  end
end
