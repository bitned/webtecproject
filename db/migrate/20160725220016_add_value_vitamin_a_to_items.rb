class AddValueVitaminAToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :valueVitaminA, :integer
  end
end
