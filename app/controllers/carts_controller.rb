class CartsController < ApplicationController
#  The following code was taken from a helpful video
#  tutorial by Youtuber Martins Tutorials. The 
#  video is part of a series of five videos
#  on how to implement cart functionality into
#  a Rails application. The video is titled
#  Ruby on Rails Shopping Cart Tutorial Adding Cart Logic 4
#  Scource:  https://www.youtube.com/watch?v=WRVwfVUaj_Y

    
        def clearcart
          session[:cart] = nil
          redirect_to cart_path
        end
    
    
    def add
        id = params[:id]
      if session[:cart] then
        cart = session[:cart]
      else
        session[:cart] = {}
        cart = session[:cart]
      end
      
      if cart[id] then
        cart[id] = cart[id] + 1
      else
        cart[id] = 1
      end
      redirect_to :action => :index
      
    end

    
    def index
         if session[:cart] then
           @cart = session[:cart]
         else
           @cart = {}
         end
    end

end
