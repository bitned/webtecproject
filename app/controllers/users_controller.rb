class UsersController < ApplicationController
    
    before_action :logged_in_user, only: [:edit, :update, :show]
    
    
    
    def index
        @users = User.all
    end
    
    def show
        @user = User.find(params[:id])
    end
    
    def new
        @user = User.new
    end
    
    def create
        @user = User.new(user_params)
        @user.save
        if @user.save
            flash[:success] = "You have signed up successfully!"
            log_in(@user)
            redirect_to @user
        else
            render 'new'
        end
    end
    
    def edit
        @user = User.find(params[:id])
    end
    
    def update
        @user = User.find(params[:id])
        if User.update(user_params)
            flash[:success] = 'You have updated your details successfully'
            render 'edit'
        else
            flash[:danger] = 'Invalid Information'
        end
    end
        
    
    
    
    
    private
        def user_params
            params.require(:user).permit(:name, :email, :password,
                                         :password_confirmation)
        end
        
        
        def logged_in_user
            unless logged_in?
                flash[:danger] = "Please log in."
                redirect_to login_url
            end
        end
    
    
end
