class ItemsController < ApplicationController
  def index
    @item = Item.paginate(page: params[:page], per_page: 9)
    if params[:search]
      @item = Item.search(params[:search]).paginate(page: params[:page],
              per_page: 9)
    end
    if params[:filter]
      @item = Item.search(params[:search]).paginate(page: params[:page],
              per_page: 9).order(price: 'asc')
    end
    if params[:sort] && params[:sort] == 'juices'
      @item = Item.paginate(page: params[:page], per_page: 9).juicesOnly
    elsif params[:sort] && params[:sort] == 'vegetables'
       @item = Item.paginate(page: params[:page], per_page: 9).veggieOnly
    elsif params[:sort] && params[:sort] == 'fruits'
      @item = Item.paginate(page: params[:page], per_page: 9).fruitsOnly
    end
    if params[:sort] && params[:sort] == 'juices' && params[:filter] &&
       params[:filter] == 'low fat'
       @item = Item.search(params[:search]).paginate(page: params[:page],
              per_page: 9).sort_low_fat
    elsif params[:sort] && params[:sort] == 'juices' && params[:filter] &&
          params[:filter] == 'no nuts'
          @item = Item.paginate(page: params[:page], per_page: 9).sort_no_nuts
    elsif params[:sort] && params[:sort] == 'juices' && params[:filter] &&
          params[:filter] == 'low sugar'
          @item = Item.paginate(page: params[:page], per_page: 9).sort_low_sugar
    end
  end

  def new
    @item = Item.new
  end

  def show
    @item = Item.find(params[:id])
  end
  
  def create
    @item = Item.new(items_params)
    @item.save
    if @item.save
      flash[:success] = 'This item has been successfully saved to the database.'
      redirect_to items_path
    else
      flash[:danger] = 'Failed save'
    end
  end

  def edit
    @item = Item.find(params[:id])
  end
  
  def update
    @item = Item.find(params[:id])
        if @item.update(items_params)
            flash[:success] = "#{@item.title} has been successfully updated."
            redirect_to items_path
        else
            flash[:danger] = "Invalid information entered"
            render 'edit'
        end
        
  end
  
  
  def questions
  end
  
  def recommendations
    
    @myTitleList = []
    @mySugarContentList = []
    @myFatLevelList = []
    
    
    @item = Item.juicesOnly
    
     @item.each do |item|
      @myTitleList << "#{item.title}"
      @mySugarContentList << "#{item.sugar_comtent}"
      @myFatLevelList << "#{item.fat_level}"
    end
  
  remove_high_fat(params[:Q3], @myTitleList)
  remove_nuts(params[:Q2], @myTitleList)
  remove_high_protein(params[:Q1], @myTitleList)
  
  end
  
  def sort
    @item = Item.sort('price')
  end


  def table
    @item = Item.all
  end
  

  
  
  
  
  
  
  
  
  private
        def items_params
            params.require(:item).permit(:title, :price, :sugar_comtent,
            :contains_nuts, :mixture, :fat_level, :description, 
            :catergory, :url, :url1, :url_2, :url_3, :url_4,
            :valueCalories, :valueFatCalories, :valueTotalFat,
            :valueSatFat, :valueTransFat, :valueCholesterol, :valueSodium,
            :valueTotalCarb, :valueFibers, :valueSugars, :valueProteins,
            :valueVitaminA, :valueVitaminC, :valueCalcium, :valueIron, 
            :ingredients, :protein_level)
        end
  
end
