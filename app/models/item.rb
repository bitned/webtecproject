class Item < ApplicationRecord
    
    validates :terms_of_service, acceptance: true
    
    
    

    def self.search(search)
            self.where('title LIKE ?', "%#{search}%")
    end
    
    
#   Methods for catergory-filtering
    def self.juicesOnly
        self.where('catergory = ?', 'Juice').order(title: "asc")
    end
    
    def self.veggieOnly
        self.where('catergory = ?', 'Vegetable').order(title: "asc")
    end
    
    def self.fruitsOnly
        self.where('catergory = ?', 'Fruit').order(title: "asc")
    end
    
    
#   Method for sorting price
    def self.sort_price
        self.where('catergory = ?', "#{params[sort]}").order(price: 'asc')
    end

#   Method for sorting high fat juices
    def self.sort_low_fat
        self.where('catergory = ?', 'Juice').order(fat_level: "asc")
    end
    
#   Method for sorting no nuts-containing juices
    def self.sort_no_nuts
        self.where('catergory = ?', 'Juice').order(contains_nuts: "asc")
    end
    
#   Method for sorting low sugar juices
    def self.sort_low_sugar
        self.where('catergory = ?', 'Juice').order(sugar_comtent: "asc")
    end
    
#   Method for finding a unknown title
    def self.find_unknown(unknown)
        self.find_by(title:("#{unknown}"))
    end
    

end
