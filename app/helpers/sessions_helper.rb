module SessionsHelper
    
    
    # Logs in the user
  def log_in(user)
    session[:user_id] = user.id
  end
  
   # Perstint log in
  def remember(user)
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end
  
  

    # Makes the loog-in user 'current user'
  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: session[:user_id])
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      if user && user.authenticated?(cookies[:remember_token])
        log_in user
        user = @current_user
      end
    end
  end
    
    # Check to see if user is logged in/if 'current user' exsits
    def logged_in?
        !current_user.nil?
    end
    
    def log_out
        session.delete(:user_id)
        @current_user = nil
        session[:user_id] = nil
    end
    
    def show_settings
        puts "Settings"
    end
end
