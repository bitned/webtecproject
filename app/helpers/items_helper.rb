module ItemsHelper
    
    def sort_out 
       
    end
    
    
#   For Question 1: Code that will remove high-protein juices
    def remove_high_protein(userAns, juiceList)
        if userAns == 'No'
            @juice = Item.juicesOnly
            @juice.each do |juice|
                if juice.protein_level > 3
                    z = juice.title
                    juiceList.delete(z)
                end
                end
        end
    end

#   For Question 2: Code that will remove nut-containing juices
    def remove_nuts(userAns, juiceList)
        if userAns == 'Yes'
            @juice = Item.juicesOnly
            @juice.each do |juice|
                if juice.contains_nuts == true
                    z = juice.title
                    juiceList.delete(z)
                end
                end
        end
    end


#   For Question 3: Code that will remove high-fat level juices
    def remove_high_fat(userAns, juiceList)
        if userAns == 'Yes'
            @juice = Item.juicesOnly
            @juice.each do |juice|
                if juice.fat_level > 3
                    z = juice.title
                    juiceList.delete(z)
                    @juice.find_unknown(z).price
                end
                end
        end
    end
    
    def title_price(itobj)
        itobj.title + " - €#{itobj.price}"
    end
end
