module CartsHelper
    
    def cart_empty?
        return true if session[:cart].nil?
    end
    
end
